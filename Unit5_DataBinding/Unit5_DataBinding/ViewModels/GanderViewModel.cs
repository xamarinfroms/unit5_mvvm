﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;

namespace Unit5_DataBinding
{
   public  class GenderViewModel
    {
        public string DisplayName { get; set; }
        public Gender Gander { get; set; }
    }
}
