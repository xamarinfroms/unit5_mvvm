﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinUniversity.Infrastructure;
using XamarinUniversity.Interfaces;
using XamarinUniversity.Services;

namespace Unit5_DataBinding
{
    public class LoginViewModel: SimpleViewModel
    {
        public LoginViewModel()
        {
            this.serviceLocator = new DependencyServiceWrapper();
        }

        public async void GoInfo()
        {
            await serviceLocator.Get<INavigationService>().NavigateAsync("Info", new InfoViewModel());
        }

        private IDependencyService serviceLocator;
    }
}
