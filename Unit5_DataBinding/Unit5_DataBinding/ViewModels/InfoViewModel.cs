﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Unit5.Core;
using XamarinUniversity.Infrastructure;

namespace Unit5_DataBinding
{
    public class InfoViewModel: INotifyPropertyChanged
    {

        public InfoViewModel()
        {
            this.GanderList = new List<GenderViewModel>() {
                new GenderViewModel() { Gander = Gender.Boy, DisplayName = "男" },
                     new GenderViewModel() { Gander = Gender.Girl, DisplayName = "女" } };
        }


        public string ImagePath
        {
            get { return this._model.ImagePath; }
        }

        public bool IsBonusGreaterZero
        {
            get { return this._model.Bonus > 0; }
        }

        public int Bonus
        {
            get
            {
                return this._model.Bonus;
            }
            set
            {
                this._model.Bonus = value;
                this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.Bonus)));
                this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.IsBonusGreaterZero)));
            }
        }

        public string Name
        {
            get { return this._model.Name; }
            set { this._model.Name = value; this.PropertyChanged?.DynamicInvoke(this,new PropertyChangedEventArgs( nameof(this.Name))); }
        }

        public string Email
        {
            get { return this._model.Email; }
            set { this._model.Email = value; this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.Email))); }
        }

        public int GanderIndex
        {
            get { return this.GanderList.IndexOf(c => c.Gander == this._model.Gender); }
            set {
                this._model.Gender = this.GanderList.ElementAt(value).Gander;
                this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.GanderIndex)));
            }
        }

        public DateTime Birthday
        {
            get { return this._model.Birthday; }
            set
            {
                _model.Birthday = value;
                this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.Birthday)));
            }
        }

        public IEnumerable<GenderViewModel> GanderList
        {
            get;
        }


        private InfoData _model = new InfoData();

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
